# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

class HomepageSlideshow(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    title = models.CharField(max_length=150)
    image = models.FileField(max_length=150, upload_to="homepage-sliders", blank=True, null=True)
    content = RichTextField()

    def __str__(self):
        return self.title

class Model(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=150)
    gender = models.CharField(max_length=10, choices=(
        ("Male", "Male"),
        ("Female", "Female"),
        ("Transgender", "Transgender"),
    ), default="Female")
    date_of_birth = models.DateField(default=timezone.now)
    hometown = models.CharField(max_length=65, blank=True, null=True)
    country = models.CharField(max_length=50, default="India")
    about = RichTextField(blank=True, null=True)
    main_pic = models.FileField(max_length=150, upload_to="models/main-pics")

    def __str__(self):
        return self.name

class Images(models.Model):
    image = models.FileField(max_length=150, upload_to="models/extra-images")
    model = models.ForeignKey(Model, blank=True, null=True)


class Query(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    phone = models.BigIntegerField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Testiomonial(models.Model):
    client_pic = models.FileField(upload_to="testiomonials/client-pics", blank=True, null=True)
    name = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    testiomonial = models.TextField()

    def __(str):
        return self.name
