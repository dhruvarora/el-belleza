# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-25 08:28
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_homepageslideshow_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.FileField(max_length=150, upload_to='models/extra-images')),
            ],
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(max_length=150)),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female'), ('Transgender', 'Transgender')], max_length=10)),
                ('date_of_birth', models.DateField(default=django.utils.timezone.now)),
                ('hometown', models.CharField(blank=True, max_length=65, null=True)),
                ('country', models.CharField(default='India', max_length=50)),
                ('about', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('main_pic', models.FileField(max_length=150, upload_to='models/main-pics')),
                ('image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='website.Images')),
            ],
        ),
    ]
