# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import HomepageSlideshow, Model

def index(request):
    homepage_sliders = HomepageSlideshow.objects.all()
    print homepage_sliders

    return render(request, "index.html", {
    "sliders":homepage_sliders,
    })

def contact(request):
    return render(request, "contact.html", {

    })

def about(request):
    models = Model.objects.all()

    return render(request, "about.html", {
    "models":models,
    })

def models(request):
    models = Model.objects.all()
    return render(request, "models.html", {
    "models":models,
    })
