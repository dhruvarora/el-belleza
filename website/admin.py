# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import HomepageSlideshow, Images, Model

class HomepageSlideshowAdmin(admin.ModelAdmin):
    list_display = ["title", "date_added"]

class ImagesInline(admin.StackedInline):
    model = Images
    extra = 3

class ModelAdmin(admin.ModelAdmin):
    list_display = ["name", "gender", "date_added", "hometown", "country"]
    search_fields = ["name"]
    list_filter = ["gender", "country"]
    inlines = [ImagesInline]

admin.site.register(HomepageSlideshow, HomepageSlideshowAdmin)
admin.site.register(Model, ModelAdmin)
